/*
Name: Christopher Hurst
Class: CSC240
Due Date: 07/10/16
Files: BuzyBot_Calculator.class, BuzyBot_Calculator.java
Description: This program prompts the user to enter two number and then asks the 
    user if they want to add, subtract, multiply, or divide. The program once 
    commanded will give the answer to the user.
 */
package inv_tracker;

    import java.util.Scanner;
    import java.text.DecimalFormat;
    import java.util.Formatter;

public class Inv_Tracker {

    public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    DecimalFormat DF = new DecimalFormat("0.00"); 
    Inv Item = new Inv();
    nPrint('a', 4);
    System.out.println("Welcome to the Inventory Tracker Program");
    System.out.println("This program will accept the names and costs of 10 stocked items.");
    System.out.println("The program will then output a table with the names, costs, prices"
            + "\n\tof the items.");
    System.out.println("Prices are calculated with a 30% markup on cost.");
    System.out.print("\nEnter B to Begin, E to End: ");
    String data = input.next();
    data = data.toUpperCase();
    switch(data){
        case "B":
            for(int i = 0; i < 8; i++){
                System.out.print("\nEnter product " + (i+1) + " name: ");
                String item = "";
                item = input.next();
                Item.addItem(item ,i);
                System.out.print("Enter the cost of the item: ");
                double cost;
                cost = input.nextDouble();
                Item.addCost(cost, i);
            }
            Item.printInv();        
        case "E":
            break;
    }
        return;
    }
}