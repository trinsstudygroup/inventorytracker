
package inv_tracker;

import java.text.DecimalFormat;


public class Inv {

    DecimalFormat DF = new DecimalFormat("0.00");
    
private static String inv[] = new String[8];
private static double cost[] = new double[8];

public void addItem(String name, int t){
    for(int i = t; i < inv.length; i++){
        if(inv[i] == null){
            inv[i] = name;
            return;
        }
    }
    
}

public void addCost(double price, int t){
    for(int i = t; i < cost.length; i++){
        cost[i] = price;
        return;
    }
    
}
        

public void printInv(){
    
    System.out.println("\nInventory Table\n");
    System.out.printf("%s %15s %15s", "Item Name", "Item Cost", "Item Price" );
    for(int i = 0; i < inv.length; i++){
        String x = inv[i];
        double c = cost[i];
        System.out.printf("\n%-15s %-15.2f %-14.2f" , x, c, (c + (c*.30)));
        }    
    }

}


